package Bomberman;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

import Core.Board;
import Core.Window;

/**
 * 
 * @author Daniel Serrano Videojoc Bomberman
 */
public class Bomberman {

	static ArrayList<Enemigo> enemies = new ArrayList<>();
	static ArrayList<Bomb> bombs = new ArrayList<>();
	static ArrayList<Explosion> explosions = new ArrayList<>();
	static ArrayList<Posicio> powerups = new ArrayList<>();
	static boolean end = false;

	static int bombMaxUp = 0;
	static int bombRadiusUp = 0;

	public static void main(String[] args) throws InterruptedException {
		Scanner sc = new Scanner(System.in);
		Board t = new Board();
		Window f = new Window(t);
		t.setActborder(false);
		t.setColorbackground(0xFFFFFF);
		int[][] matriz = { { 1, 1, 1, 1, 1, 1, 1 }, { 1, 3, 0, 2, 0, 0, 1 }, { 1, 0, 1, 0, 1, 0, 1 },
				{ 1, 2, 0, 2, 0, 2, 1 }, { 1, 0, 1, 0, 1, 0, 1 }, { 1, 0, 0, 2, 0, 0, 1 }, { 1, 1, 1, 1, 1, 1, 1 }

		};
		int[][] matrizBombas = new int[7][7];
		int[][] matrizPowerUps = new int[7][7];
		Posicio pbomberman = new Posicio(1, 1);
		matriz[pbomberman.f][pbomberman.c] = 3;
		initEnemies(matriz);
		printMatriz(matriz);

		while (!end) {
			Thread.sleep(60);
			int maxBombs = 2 + bombMaxUp - bombs.size();
			Set<Character> instruccio = f.getKeysDown();
			pbomberman = moure(matriz, matrizBombas, pbomberman.f, pbomberman.c, instruccio);
			if (instruccio.contains('z') && maxBombs != 0) {
				addBomba(pbomberman, matrizBombas);
			}
			enemyAI(matriz, matrizBombas);
			startTicking();
			checkExploded(matriz, matrizBombas, matrizPowerUps);
			checkEnemies(matriz);
			tickExplosions(matrizBombas);
			checkEnemyCollision(pbomberman, matriz);
			obtainPowerUp(pbomberman, matrizPowerUps);
			checkVictory();
			printMatrizGrafica(matriz, matrizBombas, matrizPowerUps, t, f);
		}
	}

	/**
	 * Inicia els enemics a la matriu donada
	 * 
	 * @param matriz Matriu principal
	 * 
	 */
	private static void initEnemies(int[][] matriz) {
		enemies.add(new Enemigo(5, 5, 2));
		enemies.add(new Enemigo(1, 5, 2));

		for (Enemigo enemy : enemies) {
			matriz[enemy.getPos().f][enemy.getPos().c] = 4;
		}

	}

	/**
	 * Comproba si no hi queden enemics restants al mapa. En cas que no, es produeix
	 * la victoria.
	 */
	private static void checkVictory() {
		if (enemies.isEmpty()) {
			System.out.println("HAS GANADO");
			end = true;
		}
	}

	/**
	 * Mou als enemics del mapa de manera pseudoaleatoria.
	 * 
	 * @param matriz       Matriu principal
	 * @param matrizBombas Matriu de bombes
	 */
	private static void enemyAI(int[][] matriz, int[][] matrizBombas) {
		Random rnd = new Random();
		char[] movements = { 'w', 'a', 's', 'd' };
		enemies.forEach(enemy -> {
			if (rnd.nextInt(6) == 0) {
				Set<Character> movement = new HashSet<Character>();
				movement.add(movements[rnd.nextInt(4)]);

				enemy.setPos(moure(matriz, matrizBombas, enemy.getPos().f, enemy.getPos().c, movement));
			}
		});
	}

	/**
	 * Redueix els ticks restants de totes les bombes al mapa en 1. En cas d'arribar
	 * a 0, esclaten.
	 */
	private static void startTicking() {
		bombs.forEach(bomb -> {
			bomb.setTicks(bomb.getTicks() - 1);
			if (bomb.getTicks() == 0) {
				bomb.setExploded(true);
			}
		});
	}

	/**
	 * Afegeix una bomba a la posició donada.
	 * 
	 * @param pbomberman   Posició on la bomba sortirà
	 * @param matrizBombas La matriu on es mostraran els sprites de les bombes.
	 */
	private static void addBomba(Posicio pbomberman, int[][] matrizBombas) {
		Bomb bomb = new Bomb(pbomberman.f, pbomberman.c, 25, 1 + bombRadiusUp);
		bombs.add(bomb);
		matrizBombas[bomb.getPos().f][bomb.getPos().c] = 6;
	}

	/**
	 * Comprova, per cada bomba al mapa, si alguna ha esclatat.
	 * 
	 * @param matriz         Matriu principal
	 * @param matrizBombas   Matriu de bombes
	 * @param matrizPowerUps Matriu de powerups
	 */
	private static void checkExploded(int[][] matriz, int[][] matrizBombas, int[][] matrizPowerUps) {
		ArrayList<Bomb> explodedBombs = new ArrayList<>();
		bombs.forEach(bomb -> {
			if (bomb.hasExploded()) {
				explodedBombs.add(bomb);
				explode(bomb, matriz, matrizBombas, matrizPowerUps);
			}
		});
		bombs.removeAll(explodedBombs);
	}

	/**
	 * Esclata la bomba donada i genera diferents explosions al voltant seu.
	 * 
	 * @param bomb           La bomba que esclata
	 * @param matriz         Matriu principal
	 * @param matrizBombas   Matriu de bombes
	 * @param matrizPowerUps Matriu de powerups
	 */
	private static void explode(Bomb bomb, int[][] matriz, int[][] matrizBombas, int[][] matrizPowerUps) {
		System.out.println("BOOM");
		Posicio bpos = bomb.getPos();
		matrizBombas[bpos.f][bpos.c] = 5;
		explosions.add(new Explosion(bpos.f, bpos.c, 6));
		for (int i = 0; i <= bomb.getRadius(); i++) {
			int newPos = bpos.f - i;
			if (!isOutOfBounds(newPos, bpos.c, matrizBombas)) {
				if (matriz[newPos][bpos.c] == 1) {
					break;
				}
				checkCollision(new Posicio(newPos, bpos.c), matriz, matrizBombas, matrizPowerUps);
				explosions.add(new Explosion(newPos, bpos.c, 6));
				matrizBombas[newPos][bpos.c] = 5;
			}
		}
		for (int i = 0; i <= bomb.getRadius(); i++) {
			int newPos = bpos.f + i;
			if (!isOutOfBounds(newPos, bpos.c, matrizBombas)) {
				if (matriz[newPos][bpos.c] == 1) {
					break;
				}
				checkCollision(new Posicio(newPos, bpos.c), matriz, matrizBombas, matrizPowerUps);
				explosions.add(new Explosion(newPos, bpos.c, 6));
				matrizBombas[newPos][bpos.c] = 5;
			}
		}
		for (int i = 0; i <= bomb.getRadius(); i++) {
			int newPos = bpos.c - i;
			if (!isOutOfBounds(bpos.f, newPos, matrizBombas)) {
				if (matriz[bpos.f][newPos] == 1) {
					break;
				}
				checkCollision(new Posicio(bpos.f, newPos), matriz, matrizBombas, matrizPowerUps);
				explosions.add(new Explosion(bpos.f, newPos, 6));
				matrizBombas[bpos.f][newPos] = 5;
			}
		}
		for (int i = 0; i <= bomb.getRadius(); i++) {
			int newPos = bpos.c + i;
			if (!isOutOfBounds(bpos.f, newPos, matrizBombas)) {
				if (matriz[bpos.f][newPos] == 1) {
					break;
				}
				checkCollision(new Posicio(bpos.f, newPos), matriz, matrizBombas, matrizPowerUps);
				explosions.add(new Explosion(bpos.f, newPos, 6));
				matrizBombas[bpos.f][newPos] = 5;
			}
		}
	}

	/**
	 * Comprova la colisió d'un tile d'explosió amb altre element a les matrius. Si
	 * la colisió es entre un enemic, aquest perd 1 punt de salud. Si la colisió es
	 * entre el personatge, aquest mor.
	 * 
	 * @param p              La posició on comprovar la colisió
	 * @param matriz         Matriu principal
	 * @param matrizBombas   Matriu bombes
	 * @param matrizPowerUps Matriu powerups
	 */
	private static void checkCollision(Posicio p, int[][] matriz, int[][] matrizBombas, int[][] matrizPowerUps) {
		if (matriz[p.f][p.c] == 2) {
			System.out.println("PARED");
			matriz[p.f][p.c] = 0;
			powerUpSpawner(p, matrizPowerUps);
		} else if (matrizBombas[p.f][p.c] == 6) {
			System.out.println("BOMBA");
			bombs.forEach(bomb -> {
				if (bomb.getPos().f == p.f && bomb.getPos().c == p.c) {
					bomb.setExploded(true);
				}
			});
		} else if (matriz[p.f][p.c] == 3) {
			System.out.println("HAS MUERTO");
			end = true;
			matriz[p.f][p.c] = 0;
		} else if (matriz[p.f][p.c] == 4) {
			enemies.forEach(enemy -> {
				if (enemy.getPos().f == p.f && enemy.getPos().c == p.c) {
					enemy.setHp(enemy.getHp() - 1);
					if (enemy.getHp() == 0) {
						enemy.setAlive(false);
					}
				}
			});
		}
	}

	/**
	 * Genera de manera pseudoaleatoria un powerup al destruir una paret.
	 * 
	 * @param pos            La posició on posar el sprite del powerup
	 * @param matrizPowerUps Matriu de powerups
	 */
	private static void powerUpSpawner(Posicio pos, int[][] matrizPowerUps) {
		Random rnd = new Random();
		if (rnd.nextInt(4) == 0) {
			matrizPowerUps[pos.f][pos.c] = rnd.nextBoolean() ? 7 : 8;
			powerups.add(new Posicio(pos.f, pos.c));
		}
	}

	/**
	 * Obtenir el powerup designat quan hi ha colisió entre el personatge i aquest.
	 * 
	 * @param pbomberman     Posició on comprovar la colisió
	 * @param matrizPowerUps Matriu de powerups
	 */
	private static void obtainPowerUp(Posicio pbomberman, int[][] matrizPowerUps) {
		ArrayList<Posicio> obtainedPowerUps = new ArrayList<>();
		powerups.forEach(powerup -> {
			if (pbomberman.f == powerup.f && pbomberman.c == powerup.c) {
				obtainedPowerUps.add(pbomberman);
				if (matrizPowerUps[powerup.f][powerup.c] == 7) {
					bombMaxUp = 1;
				} else if (matrizPowerUps[powerup.f][powerup.c] == 8) {
					bombRadiusUp = 1;
				}
				matrizPowerUps[powerup.f][powerup.c] = 0;
			}
		});
		powerups.removeAll(obtainedPowerUps);
	}

	/**
	 * Comprova si hi ha colisió entre el personatge i un enemic. En cas que si, hi
	 * ha derrota.
	 * 
	 * @param pbomberman Posició on comprovar la colisió
	 * @param matriz     Matriu principal
	 */
	private static void checkEnemyCollision(Posicio pbomberman, int[][] matriz) {
		enemies.forEach(enemy -> {
			if (enemy.getPos().f == pbomberman.f && enemy.getPos().c == pbomberman.c) {
				System.out.println("HAS MUERTO");
				matriz[pbomberman.f][pbomberman.c] = 4;
				end = true;
			}
		});
	}

	/**
	 * Redueix els ticks restants de totes les explosions al mapa en 1. En cas
	 * d'arribar a 0, desapareixen.
	 * 
	 * @param matrizBombas Matriu de bombes
	 */
	private static void tickExplosions(int[][] matrizBombas) {
		ArrayList<Explosion> endedExplosions = new ArrayList<>();
		explosions.forEach(explosionTile -> {
			explosionTile.setTicks(explosionTile.getTicks() - 1);
			if (explosionTile.getTicks() == 0) {
				matrizBombas[explosionTile.getPos().f][explosionTile.getPos().c] = 0;
				endedExplosions.add(explosionTile);
			}
		});
		explosions.removeAll(endedExplosions);
	}

	/**
	 * Comprova, per cada enemic, si aquest esta viu o no.
	 * 
	 * @param matriz Matriu principal
	 */
	private static void checkEnemies(int[][] matriz) {
		ArrayList<Enemigo> deadEnemies = new ArrayList<>();
		enemies.forEach(enemy -> {
			if (!enemy.isAlive()) {
				deadEnemies.add(enemy);
				matriz[enemy.getPos().f][enemy.getPos().c] = 0;
			}
		});
		enemies.removeAll(deadEnemies);
	}

	/**
	 * Funció principal del moviment del personatge i els enemics. Quan la
	 * instrucció conte una W, el pj/enemic es mou adalt, A esquerra, S a sota, D
	 * dreta.
	 * 
	 * @param matriz       Matriu principal
	 * @param matrizBombas Matriu de bombes
	 * @param h            Posició horizontal
	 * @param v            Posició vertical
	 * @param instruccio   Instrucció donada (w,a,s,d)
	 * @return La nova posició on s'ha mogut
	 */
	private static Posicio moure(int[][] matriz, int[][] matrizBombas, int h, int v, Set<Character> instruccio) {
		int pj = matriz[h][v];
		if (instruccio.contains('w')) {
			matriz[h][v] = 0;
			h--;
			if (isOutOfBounds(h, v, matriz) || matriz[h][v] == 1 || matriz[h][v] == 2 || matrizBombas[h][v] != 0) {
				h++;
				matriz[h][v] = pj;
			} else {
				matriz[h][v] = pj;
			}

		} else if (instruccio.contains('d')) {
			matriz[h][v] = 0;
			v++;
			if (isOutOfBounds(h, v, matriz) || matriz[h][v] == 1 || matriz[h][v] == 2 || matrizBombas[h][v] != 0) {
				v--;
				matriz[h][v] = pj;
			} else {
				matriz[h][v] = pj;
			}

		} else if (instruccio.contains('a')) {
			matriz[h][v] = 0;
			v--;
			if (isOutOfBounds(h, v, matriz) || matriz[h][v] == 1 || matriz[h][v] == 2 || matrizBombas[h][v] != 0) {
				v++;
				matriz[h][v] = pj;
			} else {
				matriz[h][v] = pj;
			}

		} else if (instruccio.contains('s')) {
			matriz[h][v] = 0;
			h++;
			if (isOutOfBounds(h, v, matriz) || matriz[h][v] == 1 || matriz[h][v] == 2 || matrizBombas[h][v] != 0) {
				h--;
				matriz[h][v] = pj;
			} else {
				matriz[h][v] = pj;
			}
		}
		Posicio p = new Posicio(h, v);
		return p;
	}

	/**
	 * Actualitza la matriu i la dibuixa a la finestra.
	 * 
	 * @param matriz         Matriu principal
	 * @param matrizBombas   Matriu bombes
	 * @param matrizPowerUps Matriu powerups
	 * @param t              El taulell que conté les matrius
	 */
	private static void printMatrizGrafica(int[][] matriz, int[][] matrizBombas, int[][] matrizPowerUps, Board t) {
		String[] imatges = { "", "p1.png", "t1.png", "b1.png", "g1.png", "e.png", "b2.png", "pw1.png", "pw2.png" };
		// 0 - nada, 1 - pared, 2- pared rompible, 3- personaje, 4- enemigo, 5-
		// explosion, 6- bomba, 7- power up 1, 8- power up 2
		t.setSprites(imatges);
		int[][][] cosa = new int[3][7][7];
		cosa[0] = matrizPowerUps;
		cosa[1] = matriz;
		cosa[2] = matrizBombas;
		t.draw(cosa);

	}

	/**
	 * Imprimeix la matriu en consola.
	 * 
	 * @param matriz Qualsevol matriu
	 */
	private static void printMatriz(int[][] matriz) {
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz[0].length; j++) {
				System.out.print(matriz[i][j] + " ");
			}
			System.out.println();
		}
	}

	/**
	 * Comprova si una posició donada està fora del rang de la matriu.
	 * 
	 * @param f      Posició horizontal
	 * @param c      Posició vertical
	 * @param matriz Qualsevol matriu
	 * @return True si s'ha sortit. False en cas contrari.
	 */
	public static boolean isOutOfBounds(int f, int c, int[][] matriz) {
		if (f < 0 || c < 0 || f >= matriz.length || c >= matriz[0].length) {
			return true;
		}
		return false;
	}
}
