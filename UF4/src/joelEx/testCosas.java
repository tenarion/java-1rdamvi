package joelEx;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class testCosas {

	@Test
	void test() {
		String[] out = {
				"0 0 0 X X X X X X X \n" + "0 0 0 X X X X X X X \n" + "1 1 1 0 X X X X X 0 \n"
						+ "0 2 1 0 0 X X X 0 0 \n" + "0 0 0 0 0 0 0 0 2 0 \n" + "0 0 0 0 0 0 0 1 1 0 \n"
						+ "0 0 0 4 5 0 0 0 0 0 \n" + "3 4 2 5 3 2 0 0 0 0 \n" + "2 3 4 3 2 3 3 0 0 0 \n"
						+ "5 3 2 5 3 3 4 4 0 0 \n" + "PUNTS: 18 POSICIO: 0 6",
				"1 1 1 1 2 1 1 0 0 0 \n" + "1 2 3 4 0 0 0 0 0 0 \n" + "2 3 3 0 0 0 0 0 0 0 \n"
						+ "0 0 0 0 5 0 0 0 0 0 \n" + "0 0 0 0 0 0 0 0 0 0 \n" + "0 0 0 0 0 0 0 1 0 0 \n"
						+ "0 0 0 0 2 1 2 1 1 0 \n" + "3 0 0 0 1 1 0 1 X X \n" + "3 4 0 0 0 0 2 X 4 X \n"
						+ "5 3 0 0 0 0 1 X X 5 \n" + "PUNTS: 16 POSICIO: 9 9",
				"0 X X X X X X X X X \n" + "0 X X X X X X X X X \n" + "0 X X X X X X X X X \n"
						+ "3 3 X X X X X X X 3 \n" + "3 3 4 X X X X X 3 2 \n" + "0 0 3 5 X X X 4 5 0 \n"
						+ "0 0 0 4 4 4 4 5 3 0 \n" + "0 0 0 3 3 3 3 3 0 0 \n" + "0 0 0 0 2 2 3 0 0 0 \n"
						+ "0 0 0 0 0 0 0 0 0 0 \n" + "PUNTS: 158 POSICIO: 1 5",
				"0 0 0 0 0 0 0 0 0 0 \n" + "0 0 0 0 0 0 0 0 0 0 \n" + "0 0 X X X 0 0 0 0 0 \n"
						+ "0 X X X X X 0 0 0 0 \n" + "0 X X X X X 0 0 0 0 \n" + "0 X X X X X 0 0 0 0 \n"
						+ "0 0 X X X 0 0 0 0 0 \n" + "0 0 0 0 0 0 0 0 0 0 \n" + "0 0 0 0 0 0 0 0 0 0 \n"
						+ "0 0 0 0 0 0 0 0 0 0 \n" + "PUNTS: 13 POSICIO: 4 3",
				"X X X X X 0 0 0 0 0 \n" + "X X X X X 0 0 0 0 0 \n" + "X X X X 0 0 0 0 0 0 \n"
						+ "X X X 0 0 0 0 0 0 0 \n" + "X X 0 0 0 0 0 0 0 0 \n" + "0 0 0 0 0 0 0 0 0 0 \n"
						+ "0 0 0 0 0 0 0 0 0 0 \n" + "0 0 0 0 0 0 0 0 0 0 \n" + "0 0 0 0 0 0 0 0 0 0 \n"
						+ "0 0 0 0 0 0 0 0 0 0 \n" + "PUNTS: 0 POSICIO: 0 0",
				"X X X 1 1 1 1 1 1 1 \n" + "X X X 1 1 1 1 1 1 1 \n" + "X X X 1 1 1 1 1 1 1 \n"
						+ "1 1 1 1 1 1 1 1 1 1 \n" + "1 1 1 1 1 1 1 1 1 1 \n" + "1 1 1 1 1 1 1 1 1 1 \n"
						+ "1 1 1 1 1 1 1 1 1 1 \n" + "1 1 1 1 1 1 1 1 1 1 \n" + "1 1 1 1 1 1 1 1 1 1 \n"
						+ "1 1 1 1 1 1 1 1 1 1 \n" + "PUNTS: 9 POSICIO: 1 1",
				"X X X X 0 0 0 0 0 0 \n" + "X X X X X 0 0 0 0 0 \n" + "X X X X X X 0 0 0 0 \n"
						+ "X X X X X X 2 2 0 0 \n" + "X X X X X X 1 2 0 0 \n" + "X X X X X 1 1 2 0 0 \n"
						+ "X X X X 2 2 2 2 0 0 \n" + "X X X 0 0 0 0 0 0 0 \n" + "0 0 0 0 0 0 0 0 0 0 \n"
						+ "0 0 0 0 0 0 0 0 0 0 \n" + "PUNTS: 21 POSICIO: 3 1",
				"5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n"
						+ "5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n"
						+ "5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n"
						+ "5 5 5 5 5 5 5 5 5 5 \n" + "PUNTS: 0 POSICIO: 0 0",
				"5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n"
						+ "5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n"
						+ "5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n"
						+ "0 5 5 5 5 5 5 5 5 5 \n" + "PUNTS: 0 POSICIO: 0 0",
				"5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n"
						+ "5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n"
						+ "5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n" + "5 5 5 5 5 5 5 5 5 5 \n"
						+ "5 5 5 5 5 5 5 5 5 X \n" + "PUNTS: 1 POSICIO: 7 9",
				"0 0 0 0 0 0 0 0 0 0 \n" + "0 0 0 0 0 0 0 0 0 0 \n" + "0 0 0 0 0 0 0 0 0 0 \n"
						+ "0 0 0 0 0 0 X X X 0 \n" + "0 0 0 0 0 0 X X X 0 \n" + "0 0 0 0 0 0 X X X 0 \n"
						+ "0 0 0 0 0 0 0 0 0 0 \n" + "0 0 0 0 0 0 0 0 0 0 \n" + "0 0 0 0 0 0 0 0 0 0 \n"
						+ "0 0 0 0 0 0 0 0 0 0 \n" + "PUNTS: 5 POSICIO: 4 7",
				"X X X X X X X X X X \n" + "X X X X X X X X X X \n" + "X X X X X X X X X X \n"
						+ "X X X X X X X X X X \n" + "X X X X X X X X X X \n" + "X X X X X X X X X X \n"
						+ "X X X X X X X X X X \n" + "X X X X X X X X X X \n" + "X X X X X X X X X X \n"
						+ "X X X X X X X X X X \n" + "PUNTS: 500 POSICIO: 0 0" };

		for (int i = 0; i < out.length; i++) {
			assertEquals(out[i], ElBombardeoDeKahTaluniah2.solve());
		}
	}

}
