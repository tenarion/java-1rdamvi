package joelEx;

import java.util.Scanner;

public class ElBombardeoDeKahTaluniah {
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		solve();
	}

	private static void solve() {
		int ca = sc.nextInt();
		for (int i = 0; i < ca; i++) {
			int arcanium = sc.nextInt(), radius = sc.nextInt();
			int[][] map = createMap(10, 10);
			Bomba b = new Bomba(arcanium, radius);
			b.explode(map);
			printMap(map);
			System.out.println(
					"PUNTS: " + b.getPoints() + " POSICIO: " + b.getBestPosition()[0] + " " + b.getBestPosition()[1]);
		}
	}

	private static class Bomba {
		private int radius;
		private double arcanium;
		private int points;
		private double power;
		private int[] bestPosition;

		public Bomba(double arcanium, int radius) {
			this.radius = radius;
			this.arcanium = arcanium;
			this.points = 0;
			this.power = 0.0;
			this.bestPosition = new int[2];
		}

		private void explode(int[][] map) {
			for (int a = 0; a < map.length; a++) {
				for (int b = 0; b < map[0].length; b++) {
					int points = 0;
					double arcanometers = getArcanometers(map, a, b);
					double power = arcanium / arcanometers;
					this.power = power;
					for (int i = -radius; i <= radius; i++) {
						for (int j = -radius; j <= radius; j++) {
							if (i * i + j * j <= (radius * radius) + 1) {
								if (!outOfBounds(map, a + i, b + j) && power >= map[i + a][j + b]) {
									points += map[i + a][j + b];
									if (points > this.points) {
										this.points = points;
										this.bestPosition = new int[] { a, b };
									}
								}
							}
						}
					}
				}
			}
			setBombRadius(map, this.bestPosition[0], this.bestPosition[1], this.power);
		}

		private void setBombRadius(int[][] map, int a, int b, double power) {
			for (int i = -radius; i <= radius; i++) {
				for (int j = -radius; j <= radius; j++) {
					if (i * i + j * j <= (radius * radius) + 1) {
						if (!outOfBounds(map, a + i, b + j) && power >= map[i + a][j + b]) {
							map[i + a][j + b] = 9;
						}
					}
				}
			}
		}

		private int getArcanometers(int[][] map, int a, int b) {
			int arcanometers = 0;
			for (int i = -radius; i <= radius; i++) {
				for (int j = -radius; j <= radius; j++) {
					if (i * i + j * j <= (radius * radius) + 1) {
						if (!outOfBounds(map, a + i, b + j)) {
							arcanometers++;
						}
					}
				}
			}
			return arcanometers;
		}

		private int getPoints() {
			return this.points;
		}

		public int[] getBestPosition() {
			return this.bestPosition;
		}
	}

	public static boolean outOfBounds(int[][] map, int r, int c) {
		return r >= map.length || r < 0 || c >= map[0].length || c < 0;
	}

	public static int[][] createMap(int x, int y) {
		int[][] map = new int[x][y];
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[0].length; j++) {
				map[i][j] = sc.nextInt();
			}
		}
		return map;
	}

	public static void printMap(int[][] map) {
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[0].length; j++) {
				if (map[i][j] == 9) {
					System.out.print("X ");
				} else {
					System.out.print(map[i][j] + " ");
				}
			}
			System.out.println();
		}
	}
}
