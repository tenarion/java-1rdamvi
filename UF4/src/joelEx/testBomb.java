package joelEx;

public class testBomb {

	public static void main(String[] args) {
		int[][] map = new int[10][10];
		explode(map, 30, 3, 5, 5);
		printMap(map);
	}

	private static void explode(int[][] map, int arcanium, int radius, int a, int b) {

		for (int i = -radius; i <= radius; i++) {
			for (int j = -radius; j <= radius; j++) {
				if (i * i + j * j <= (radius * radius)+1) {
					if (!outOfBounds(map, a + i, b + j)) {
						map[i + a][j + b] = 1;
					}
				}
			}
		}

	}

	public static boolean outOfBounds(int[][] map, int r, int c) {
		return r >= map.length || r < 0 || c >= map[0].length || c < 0;
	}

	public static void printMap(int[][] map) {
		for (int i = 0; i < map.length; i++) {
			for (int j = 0; j < map[0].length; j++) {
				System.out.print(map[i][j] + " ");
			}
			System.out.println();
		}
	}
}
